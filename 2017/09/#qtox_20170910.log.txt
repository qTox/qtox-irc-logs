[00:54:15] *** Quits: ben-nabiy (~bennabiy@ ) (Remote host closed the connection)
[01:52:25] *** Quits: LittleVulpix (~Miranda@ ) (Quit: Miranda NG rocks. Trust the Vulpix. http://miranda-ng.org/)
[05:57:51] *** Joins: ben_nabiy (~bennabiy@ )
[06:20:16] *** Quits: ben_nabiy (~bennabiy@ ) (Quit: http://www.yellowdeli.com)
[07:06:45] *** Joins: f10 (~flo@ )
[07:24:00] <Diadlo> zetok, ping
[07:37:52] *** Joins: Diadl0 (~Diadlos2@ )
[07:37:52] *** Quits: Diadlo (~Diadlos2@ ) (Read error: Connection reset by peer)
[07:55:49] <zetok> Diadl0: did Talkless say that he wants to join maintainers?
[07:57:30] <Diadl0> zetok, Probably, not. I suggest to invite him :)
[07:59:26] <zetok> how to put it
[07:59:57] <zetok> I don't think that it's needed/beneficial/helpful
[08:00:59] *** Joins: lmposter (~lmposter@ )
[08:01:12] <zetok> from the PRs from Talkless over the years, it seems that he has fairly long-lasting interest in contributing to qTox
[08:02:21] <zetok> regarding their number, it's not much, which makes me thing that either he just wants to contribute from time to time whenever he feels like it, or contribute when he has the time to sit down and make a contribution
[08:02:29] <zetok> makes me think*
[08:06:11] <zetok> regardless of the underlying reason, making an invite to become a maintainer would imply increasing time spent on qTox tremendously – not a very good thing to bestow upon someone who either has little time to spend on contributing, or just wants to contribute a bit from time to time
[08:07:38] <zetok> now, if he did inquire about becoming a maintainer, the case would be entirely different, since it would signify that he does have interest in becoming more involved in qTox development, and thus spending a lot more time on it
[08:10:13] <zetok> but, as for underlying reason why would you ask about making an invite
[08:11:12] <zetok> Diadl0: it feels like there's not enough contributors/maintainers?
[08:12:30] <zetok> it would be good to take some action to improve things that could attract new contributors/maintainers, or improve retention of current ones
[08:12:52] <zetok> (preferably both)
[08:13:40] <Diadl0> zetok, Probably, tox dies
[08:14:10] <Diadl0> No one want to contribute to client for protocol, which haven't any progress
[08:15:34] <zetok> hmm
[08:15:58] <zetok> yeah, progress has become very slow
[08:17:17] <zetok> Hm.
[08:18:41] <zetok> alright
[08:19:22] <zetok> Diadl0: would paying developers help with development?
[08:20:58] <Diadl0> I can't guarantee anything
[08:21:55] <zetok> um, it's not about promising something
[08:22:13] <zetok> it's just a general question of whether work gets done when people are paid to do the work
[08:40:26] -qtox-git-spam- [qTox] noavarice closed pull request #4598: Removing some unused Core parts (master...refactor-core) https://git.io/v53Di
[09:51:20] *** Joins: LittleVulpix (~Miranda@ )
[10:10:34] *** Quits: cisc (~cisc@ ) (Ping timeout: 240 seconds)
[10:29:05] *** Joins: murray (~murray@ )
[10:30:43] *** Joins: cenysor (~cenysor@ )
[10:45:47] *** Joins: noavarice (~alexrazin@ )
[10:53:37] *** Joins: cisc (~cisc@ )
[11:39:06] -qtox-git-spam- [qTox] Diadlo pushed 7 new commits to master: https://git.io/v5iFM
[11:39:06] -qtox-git-spam- qTox/master c6d76da Diadlo: refactor: Use ToxPk instead of ToxId
[11:39:06] -qtox-git-spam- qTox/master 8368f24 Diadlo: refactor: Rename AboutUser to AboutFriendForm
[11:39:07] -qtox-git-spam- qTox/master b0a591e Diadlo: refactor(aboutfriend): Add AboutFriend model and interface
[11:39:52] -qtox-git-spam- [qTox] Diadlo pushed 1 new commit to master: https://git.io/v5iFH
[11:39:52] -qtox-git-spam- qTox/master bd6516e Diadlo: fix: Fix crash on auto accept click in AboutFriendForm...
[11:52:35] *** Quits: cenysor (~cenysor@ ) (Read error: Connection reset by peer)
[12:19:49] *** Quits: skyzohkey (~skyzohkey@ ) (Ping timeout: 264 seconds)
[12:23:55] *** Quits: LittleVulpix (~Miranda@ ) (Quit: Miranda NG rocks. Trust the Vulpix. http://miranda-ng.org/)
[14:12:36] *** Quits: noavarice (~alexrazin@ ) (Quit: WeeChat 1.7)
[14:14:41] *** Joins: skyzohkey (~skyzohkey@ )
[14:19:06] *** Quits: murray (~murray@ ) (Read error: No route to host)
[14:48:01] *** Joins: murray (~murray@ )
[15:12:37] *** Quits: f10 (~flo@ ) (Ping timeout: 240 seconds)
[16:28:00] *** Joins: f10 (~flo@ )
[16:50:17] *** Joins: cenysor (~cenysor@ )
[17:00:43] *** Quits: skyzohkey (~skyzohkey@ ) (Quit: Leaving)
[17:49:57] *** Quits: cisc (~cisc@ ) (Read error: Connection reset by peer)
[19:16:39] *** Joins: cisc (~cisc@ )
[19:56:06] *** Quits: s-mutin (~s-mutin@ ) (Ping timeout: 240 seconds)
[19:59:33] *** Joins: s-mutin (~s-mutin@ )
[20:05:01] *** Quits: s-mutin (~s-mutin@ ) (Ping timeout: 252 seconds)
[20:05:52] *** Joins: s-mutin (~s-mutin@ )
[20:22:41] *** Joins: noavarice (~alexrazin@ )
[20:40:22] *** Quits: cenysor (~cenysor@ ) (Remote host closed the connection)
[22:05:27] *** Quits: noavarice (~alexrazin@ ) (Quit: WeeChat 1.7)
[22:34:04] *** Quits: murray (~murray@ ) (Quit: Leaving)
[22:34:22] *** Quits: f10 (~flo@ ) (Ping timeout: 248 seconds)
[22:56:07] *** Quits: lmposter (~lmposter@ ) (Read error: Connection reset by peer)
[23:13:12] *** Joins: sudden6 (~anonymous@ )
