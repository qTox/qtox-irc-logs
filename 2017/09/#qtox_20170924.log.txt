[00:00:30] *** Quits: ghutzriop (~ghutzriop@ ) (Quit: Leaving)
[00:13:20] *** Quits: cenysor (~cenysor@ ) (Remote host closed the connection)
[00:49:38] *** Quits: sudden6 (~anonymous@ ) (Ping timeout: 240 seconds)
[00:58:06] *** Joins: ghutzriop (~ghutzriop@ )
[01:38:03] *** Parts: ghutzriop (~ghutzriop@ ) ("Leaving")
[02:31:22] <nurupo> libexif-dev
[02:32:29] <nurupo> should be in the docs too https://github.com/qTox/qTox/pull/4643/commits/414fa178b4908d8451ca5304ee9ca1ab7421b26a
[02:33:29] <nurupo> about push-to-talk functionality
[02:33:44] <nurupo> isn't Mumble a Qt app too?
[02:35:08] <nurupo> i think it *might* (didn't verify) have the push-to-talk functionality that works when Qt window is inactive/not focused. if so, qTox could just borrow the code in the best traditions of foss :P
[02:35:08] *** Quits: XenoJl (~XenoJl@ ) (Read error: Connection reset by peer)
[02:35:30] *** Joins: XenoJl (~XenoJl@ )
[05:14:51] *** Joins: Slayer_ (~slayer@ )
[08:19:41] *** Joins: f10 (~flo@ )
[09:15:48] *** Quits: Slayer_ (~slayer@ ) (Remote host closed the connection)
[09:40:47] *** Joins: murray (~murray@ )
[10:15:21] *** Joins: Mojo` (~mojo@ )
[10:15:25] <Mojo`> hi world
[10:16:15] <Mojo`> I just find a feature useful and I wonder if should open an issue for that (don't seems to exist)
[10:16:34] <Mojo`> I would need to share my GPS location with a friend or a group a friends
[10:18:46] <nurupo> you could open an issue for it
[10:18:54] <nurupo> a feature request issue
[10:20:13] <nurupo> you might want to deskribe how this feature is supposed to work in it too
[10:21:01] <nurupo> i assume you don't actually have a gps module inside your computer that qTox could query for the current coordinates, do you?
[10:21:17] <nurupo> *describe
[10:46:03] <Mojo`> nope, I don't have one
[10:47:04] <Mojo`> ok then, I will open an issue with some details about this
[10:53:33] <Mojo`> probably need to have a the feature available in the Tox core libraries first
[10:55:12] *** Joins: cenysor (~cenysor@ )
[11:01:27] <Mojo`> Ho also, to share this nice tool: http://kaitai.io/
[11:01:29] <Mojo`> think swagger description but for binary formats
[11:01:32] <Mojo`> may help
[11:38:18] *** Joins: Amuza (~Amuza@ )
[12:18:58] *** Quits: f10 (~flo@ ) (Ping timeout: 260 seconds)
[12:36:32] *** Joins: sudden6 (~anonymous@ )
[13:00:59] *** Quits: cenysor (~cenysor@ ) (Remote host closed the connection)
[13:11:59] *** Joins: f10 (~flo@ )
[13:33:16] *** Quits: f10 (~flo@ ) (Ping timeout: 240 seconds)
[13:37:08] *** Joins: f10 (~flo@ )
[14:27:13] -qtox-git-spam- [qTox] GergelySzekely opened pull request #4684: fixing a broken link (master...master) https://git.io/vdTL9
[14:53:07] *** Joins: Achylles (~Achylles@ )
[14:55:31] *** Joins: kkaufmann (~ghutzriop@ )
[14:55:56] *** Joins: mutin-s (~s-mutin@ )
[14:58:21] *** Quits: s-mutin (~s-mutin@ ) (Ping timeout: 240 seconds)
[14:59:36] *** Quits: XenoJl (~XenoJl@ ) (Ping timeout: 240 seconds)
[15:51:34] *** Joins: cenysor (~cenysor@ )
[16:28:39] *** Quits: f10 (~flo@ ) (Ping timeout: 252 seconds)
[16:59:27] *** Quits: Amuza (~Amuza@ ) (Ping timeout: 252 seconds)
[17:14:49] *** Joins: Amuza (~Amuza@ )
[17:22:04] *** Joins: Bill_MI (~Bill@ )
[17:25:19] *** Joins: f10 (~flo@ )
[17:32:00] *** Quits: Bill_MI (~Bill@ ) (Quit: Bye...)
[17:35:28] *** Quits: Achylles (~Achylles@ ) (Ping timeout: 240 seconds)
[17:45:11] *** Quits: f10 (~flo@ ) (Ping timeout: 255 seconds)
[17:50:09] *** Quits: Amuza (~Amuza@ ) (Ping timeout: 248 seconds)
[17:58:48] *** Quits: sudden6 (~anonymous@ ) (Ping timeout: 240 seconds)
[18:11:47] *** Joins: sudden6 (~anonymous@ )
[19:08:02] *** Joins: f10 (~flo@ )
[19:33:20] *** Quits: f10 (~flo@ ) (Ping timeout: 240 seconds)
[19:58:45] *** Quits: cenysor (~cenysor@ ) (Remote host closed the connection)
[20:01:59] *** Joins: Amuza (~Amuza@ )
[20:37:54] *** Joins: f10 (~flo@ )
[20:39:20] <kkaufmann> what would be the approach to write test cases that depend on disconnects?
[20:41:38] *** Quits: Amuza (~Amuza@ ) (Ping timeout: 252 seconds)
[21:03:32] *** Quits: Chiitoo (~Chiitoo@ ) (Quit: Sayonara sandwich!)
[21:24:56] <sudden6> kkaufmann: uhm, what do you want to test?
[21:26:19] <sudden6> zetok: PRs adding translateable strings shouldn update translation files, right?
[21:28:35] <sudden6> *shouldn't
[21:29:02] <zetok> sudden6: yeah
[21:29:18] <sudden6> ok
[21:29:39] <kkaufmann> sudden6, all sequences that users could do in regards to resuming filetransfers. One reason is, that I'm really struggling with a specific bug and always going trough the interface is tedious
[21:29:56] <sudden6> zetok: do you have time to handle the translations stuff for the next release on sunday?
[21:30:12] <zetok> sudden6: yeah
[21:30:25] <sudden6> zetok: great
[21:31:50] <zetok> I'll make a PR with translations on Friday/Saturday
[21:33:02] <sudden6> kkaufmann: I don't really have an idea how to do that, maybe ask Diadlo, he is introducing testing to qTox 
[21:34:45] <kkaufmann> sudden6, on the other hand, it could be something similar simple like "auto file = i.value(); file.fileNum = fileNum"
[21:39:56] *** Joins: anthonybilinski (45aca9af@ )
[21:41:29] <anthonybilinski> does anyone know what toxcore-ngc is? I see it in the build scripts but can't find anything about what it
[21:48:45] <sudden6> anthonybilinski: huh? qTox build scripts?
[22:01:18] <anthonybilinski> well, the packaging scripts
[22:03:47] *** Joins: flobe (~flobe@ )
[22:11:17] *** Joins: Achylles (~Achylles@ )
[22:22:55] <sudden6> no idea then^^
[22:23:40] *** Quits: murray (~murray@ ) (Quit: Leaving)
[22:25:44] *** Quits: Achylles (~Achylles@ ) (Ping timeout: 240 seconds)
[23:05:24] *** Quits: flobe (~flobe@ ) (Quit: Bye)
[23:12:20] *** Quits: f10 (~flo@ ) (Ping timeout: 252 seconds)
[23:41:30] *** Quits: sudden6 (~anonymous@ ) (Ping timeout: 240 seconds)
