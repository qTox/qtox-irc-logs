[00:32:36] <Achylles> I have added a repo to install qtox, but the repo can only install utox, but it says qtox can be installed...
[00:32:42] <Achylles> confusing...
[00:33:18] <Achylles> deb https://pkg.tox.chat/debian/ nightly release
[00:43:57] *** Quits: sudden6 (~anonymous@ ) (Ping timeout: 240 seconds)
[00:44:48] <Achylles> https://wiki.tox.chat/clients/qtox
[02:20:05] <nurupo> Achylles: that's outdated
[02:20:52] <nurupo> long story short, there are no up-to-date qtox packages atm
[02:21:42] <nurupo> https://qtox.github.io/blog/2017/02/15/Maintainer-Wanted.html
[02:22:10] <Achylles> nurupo, if it is outdated, so it should be removed or, at least have a note there...
[02:23:06] <nurupo> Achylles: wiki maintainers wanted :P
[02:23:31] <Achylles> Anyways, I already managed to install qtox from an Antonbatenev opensuse repo or something...
[02:23:54] <nurupo> right, but that's very old qtox version
[02:24:16] <nurupo> explained i nthe blog post ^
[02:24:59] <Achylles> https://software.opensuse.org/download.html?project=home%3Aantonbatenev%3Atox&package=qtox
[02:31:57] <Achylles> Is this old? -> You are using qTox version v1.6.0-obs
[02:39:58] *** Joins: anthonybilinski (45aca9af@ )
[02:41:14] <nurupo> ><nurupo> right, but that's very old qtox version
[02:42:16] <nurupo> the latest is v1.11.0, released on July 16th
[02:53:53] <Achylles> git checkout v1.11.0 error: pathspec 'v1.11.0' did not match any file(s) known to git
[02:57:09] <Achylles> sudo ./autogen.sh
[02:57:27] <Achylles> Running autoreconf -if...
[02:57:44] <Achylles> libtoolize: $pkgltdldir is not a directory: `/usr/local/share/libtool'
[02:57:57] <Achylles> autoreconf: libtoolize failed with exit status: 1
[02:58:44] <Achylles> nurupo, what am i doing wrong?
[02:59:40] <nurupo> what are you trying to build?
[02:59:52] <nurupo> qtox doesn't use autotools...
[03:00:27] <Achylles> I am trying to compile qtox in my debian
[03:00:45] <Achylles> https://github.com/qTox/qTox/blob/master/INSTALL.md#linux
[03:02:15] <nurupo> oh, so you were trying to build toxcore?
[03:02:31] <nurupo> there is no v1.11.0 toxcore, it's qtox that is v1.11.0
[03:03:21] <Achylles> I understood that I need to compile toxcore first and then compile qtox...
[03:04:09] <Achylles> But, not working here...
[03:04:17] <Achylles> :(
[03:39:43] *** Quits: Achylles (~Achylles@ ) (Ping timeout: 264 seconds)
[03:52:35] *** Quits: anthonybilinski (45aca9af@ ) (Quit: Page closed)
[05:31:02] *** Quits: vindelschtuffen (~vindelsch@ ) (Remote host closed the connection)
[06:29:36] *** Quits: cisc (~cisc@ ) (Ping timeout: 248 seconds)
[06:32:11] *** Joins: cisc (~cisc@ )
[07:32:34] *** Joins: f10 (~flo@ )
[07:36:33] *** Quits: cisc (~cisc@ ) (Ping timeout: 240 seconds)
[07:39:07] *** Joins: cisc (~cisc@ )
[07:52:40] *** Joins: igajsin (~ugoday@ )
[08:45:10] *** Joins: lmposter (~lmposter@ )
[08:47:01] *** Joins: rokups (uid197268@ )
[09:15:06] *** Quits: f10 (~flo@ ) (Ping timeout: 264 seconds)
[09:27:47] *** Joins: murray (~murray@ )
[09:59:00] *** Joins: sudden6 (~anonymous@ )
[10:08:27] *** Joins: f10 (~flo@ )
[10:17:20] -qtox-git-spam- [qTox] sudden6 pushed 2 new commits to master: https://git.io/v5dFz
[10:17:20] -qtox-git-spam- qTox/master 48d65c2 anthony.bilinski: feat(ui): output instructions when user tries to open a second instance...
[10:17:20] -qtox-git-spam- qTox/master d746ea9 sudden6: Merge pull request #4650...
[10:23:44] *** Quits: sudden6 (~anonymous@ ) (Ping timeout: 246 seconds)
[10:26:43] *** Joins: sudden6 (~anonymous@ )
[10:42:00] *** Joins: body (~body@ )
[10:42:09] <body> hello, is someone here ? :)
[10:46:40] *** Quits: murray (~murray@ ) (Quit: Leaving)
[10:55:30] <oranges> no
[11:14:02] *** Joins: cenysor (~cenysor@ )
[12:24:52] *** Quits: cenysor (~cenysor@ ) (Remote host closed the connection)
[14:00:22] *** Quits: rokups (uid197268@ ) (Quit: Connection closed for inactivity)
[15:29:03] *** Joins: cenysor (~cenysor@ )
[16:33:58] *** Joins: murray (~murray@ )
[16:36:33] <sudden6> body: yes
[16:38:09] *** Joins: Achylles (~Achylles@ )
[16:43:52] *** Quits: cisc (~cisc@ ) (Ping timeout: 240 seconds)
[16:45:00] <Chiitoo> sudden6: I don't suppose you have seen reports of qTox suddenly hogging upload bandwidth (all it can on a 1Mbit connection), with hundreds of different addresses shown in the connection list?  Addresses like 'groupbot.services.tox.chat' are there too, even though no group chats are being used.  This is from a friend using a Windows build of qTox, I have never seen it myself happen (yet).
[16:45:37] <sudden6> Chiitoo: no, I haven't
[16:46:25] *** Joins: cisc (~cisc@ )
[16:46:45] <sudden6> Impyy:  does groupbot.services.tox.chat even exist?
[16:47:06] <sudden6> Chiitoo: are there any special circumstances
[16:47:39] <Impyy> sudden6, apparently
[16:47:57] <sudden6> is it the official groupbot?
[16:48:41] <Chiitoo> None that we've been able to spot.  It /might/ have been triggered when we have done file transfers, but it isn't consistent enough for that to surely have been the case.  Need to try in a virtual machine myself probably to get a good look.
[16:48:58] <Chiitoo> (Although I imagine others would have reported it already, too, if it was widespread.)
[16:49:15] <sudden6> Chiitoo: if you did file transfers it could be something like a stuck transfer...
[16:49:29] <sudden6> they are not very stable IIRC
[16:49:46] <Chiitoo> Hmhm.
[16:49:47] <sudden6> although I personally I didn't have much problems
[16:49:53] <Impyy> sudden6, I think so, but I don't manage groupbot
[16:50:21] <sudden6> Impyy: oh sorry then, who manages it?
[16:50:38] <Impyy> sudden6, jfreegman
[16:51:07] <sudden6> thx
[16:51:12] <sudden6> I'll ask him
[16:51:34] <Impyy> cool
[16:55:32] <Chiitoo> It does make one wonder what it is sending to all those addresses.  :V
[16:56:33] *** Quits: lmposter (~lmposter@ ) (Ping timeout: 260 seconds)
[16:57:52] *** Joins: lmposter (~lmposter@ )
[17:12:01] <sudden6> Chiitoo: I guess it's TCP mode or DHT traffic
[17:14:12] *** Quits: Achylles (~Achylles@ ) (Remote host closed the connection)
[17:31:32] <Chiitoo> sudden6: DHT is what I was thinking of too.
[17:33:28] <sudden6> the unusual thing is that DHT is normally in the KB/s range, which doesn't saturate 1Mbit of upload
[17:34:32] <sudden6> an outgoing filetransfer via a TCP node is more likely, but then the high traffic would only go to one node I think
[17:35:24] <Chiitoo> The highest in a picture I got from them is at 8573 B/sec (according to Windows 10 task manager I think).  That one is actually me, though I think our transfers were done by that point...  The next connections are 6k, 6k, 4k, 3, 2, 770, 600... etc. diminishing numbres from there.
[17:36:20] <sudden6> KBit/s or KBytes/s?
[17:36:48] <Chiitoo> The colum is “Send (B/sec) so Bytes maybe?
[17:37:28] <sudden6> probably
[17:38:04] <Chiitoo> Receive is 0 for every single one of them, only sending has traffic.
[17:38:20] <Chiitoo> According to the friend, yeah, bytes.
[17:38:47] <sudden6> strange, which version of toxcore is this? maybe try newest 1.10?
[17:40:42] <Chiitoo> 1.10 or 0.1.10?  Hmmm yeah, it's slightly behind by now, at 0.1.9.
[17:40:53] <robinli> 0.1.10.
[17:40:53] <sudden6> 0.1.10
[17:41:00] <Chiitoo> :]
[17:41:14] <robinli> No major differences between 0.1.9 and 0.1.10 though. No bugfixes at least.
[17:41:21] <robinli> Just that one data reduction PR.
[17:41:23] <sudden6> I think there were bandwith fixes in 0.1.10?
[17:41:41] <robinli> Bandwidth reduction stuff. It changed the onion ping timings I think.
[17:41:56] <robinli> It wouldn't have fixed ^.
[17:43:12] <robinli> https://github.com/TokTok/c-toxcore/pull/542
[17:47:25] <Chiitoo> Hmmhm.
[18:12:47] -qtox-git-spam- [qTox] noavarice pushed 4 new commits to master: https://git.io/v5F30
[18:12:47] -qtox-git-spam- qTox/master ad3584c noavarice: refactor: GroupInvite moved to model directory
[18:12:47] -qtox-git-spam- qTox/master fd1fbeb noavarice: refactor: more rigorous types for GroupInvite
[18:12:47] -qtox-git-spam- qTox/master d514b48 noavarice: refactor: register GroupInvite as meta-type
[18:13:10] *** Joins: noavarice (5f200be5@ )
[18:31:03] *** Quits: f10 (~flo@ ) (Ping timeout: 260 seconds)
[18:41:30] *** Joins: f10 (~flo@ )
[19:20:02] *** Quits: igajsin (~ugoday@ ) (Remote host closed the connection)
[19:34:42] *** Quits: noavarice (5f200be5@ ) (Quit: Page closed)
[19:51:11] *** Quits: f10 (~flo@ ) (Ping timeout: 240 seconds)
[20:25:29] *** Joins: f10 (~flo@ )
[20:54:36] *** Quits: cenysor (~cenysor@ ) (Remote host closed the connection)
[21:51:46] *** Quits: murray (~murray@ ) (Quit: Leaving)
[22:01:19] *** Quits: f10 (~flo@ ) (Ping timeout: 248 seconds)
[22:12:20] -qtox-git-spam- [qTox] sudden6 pushed 2 new commits to master: https://git.io/v5FRx
[22:12:20] -qtox-git-spam- qTox/master e9d6339 anthony.bilinski: fix(receipts): Prevent double message send for received receipt...
[22:12:20] -qtox-git-spam- qTox/master 9c01eec sudden6: Merge pull request #4607...
[23:26:18] *** Quits: sudden6 (~anonymous@ ) (Ping timeout: 240 seconds)
[23:43:18] *** Quits: lmposter (~lmposter@ ) (Ping timeout: 240 seconds)
