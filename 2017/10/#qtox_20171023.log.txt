[00:53:45] *** Quits: f10 (~flo@ ) (Ping timeout: 252 seconds)
[07:59:08] *** Joins: f10 (~flo@ )
[08:47:57] *** Joins: Xeno_PL (~Xeno@ )
[10:07:08] *** Joins: blap (~blap@ )
[10:38:15] *** Parts: blap (~blap@ ) ()
[11:30:25] <VVD> hi
[11:30:36] <VVD> what video codec used in qTox?
[11:30:42] <VVD> codes?
[11:31:34] <VVD> is it possible to force pick it and other options of the video codes in qtox?
[11:32:00] <VVD> s/codes/codecs/
[11:35:21] <VVD> opus and vp8…
[12:01:00] *** Joins: sudd3n6 (~anonymous@ )
[12:34:05] -qtox-git-spam- [qTox] sudden6 pushed 2 new commits to master: https://git.io/vdxkA
[12:34:05] -qtox-git-spam- qTox/master b1f40de anthony.bilinski: fix(ui): Set tooltip colour and background for all tooltips...
[12:34:05] -qtox-git-spam- qTox/master 923fd48 sudden6: Merge pull request #4748...
[12:36:34] -qtox-git-spam- [qTox] sudden6 pushed 2 new commits to master: https://git.io/vdxI3
[12:36:34] -qtox-git-spam- qTox/master 1613044 anthony.bilinski: fix(video): Set toxav video bitrate to 0 if answering audio-only call...
[12:36:34] -qtox-git-spam- qTox/master 1d79019 sudden6: Merge pull request #4747...
[12:38:30] -qtox-git-spam- [qTox] sudden6 closed pull request #4750: fix(build): install libexif-dev on apt-based systems (master...fix-libexif) https://git.io/vdbpV
[12:57:03] *** Joins: monty-c_ (~montyc@ )
[12:57:05] *** Parts: monty-c_ (~montyc@ ) ()
[13:08:36] *** Joins: monty-c_ (~montyc@ )
[13:56:11] -qtox-git-spam- [qTox] sudden6 closed pull request #4756: fix(video): reduce default video bitrate to make it more usable (master...reduce_video_bitrate) https://git.io/vdAf5
[13:59:41] *** Quits: f10 (~flo@ ) (Ping timeout: 240 seconds)
[14:00:13] *** Quits: sudd3n6 (~anonymous@ ) (Ping timeout: 255 seconds)
[14:10:49] *** Joins: f10 (~flo@ )
[14:32:14] *** Quits: vindelschtuffen_ (~vindelsch@ ) (Ping timeout: 264 seconds)
[14:33:32] *** Parts: cisc (~cisc@ ) ("Konversation terminated!")
[14:39:31] *** Quits: arza (znc@ ) (Excess Flood)
[14:39:43] *** Joins: arza (znc@ )
[14:51:50] *** Joins: cisc (~cisc@ )
[15:23:49] *** Joins: cenysor (~cenysor@ )
[15:27:58] *** Joins: vindelschtuffen_ (~vindelsch@ )
[15:38:09] *** Quits: vindelschtuffen_ (~vindelsch@ ) (Ping timeout: 246 seconds)
[15:55:42] *** Joins: sudd3n6 (~anonymous@ )
[16:16:00] *** Quits: cenysor (~cenysor@ ) (Remote host closed the connection)
[16:54:05] *** Quits: monty-c_ (~montyc@ ) (Quit: Leaving.)
[17:06:50] *** Joins: lmposter (~lmposter@ )
[17:12:00] *** Joins: jumikan (4f4eb6fe@ )
[17:12:24] *** Quits: albel727 (~albel727@ ) (Ping timeout: 240 seconds)
[17:30:03] <jumikan> hello
[17:39:13] *** Quits: Xeno_PL (~Xeno@ ) (Ping timeout: 240 seconds)
[17:46:01] *** Joins: Xeno_PL (~Xeno@ )
[17:52:27] *** Quits: Xeno_PL (~Xeno@ ) (Ping timeout: 240 seconds)
[18:10:17] *** sudd3n6 is now known as sudden6
[18:10:22] <sudden6> jumikan: hello
[18:10:38] <jumikan> hi sudden6 
[18:10:54] <jumikan> Are you  a coder?
[18:11:02] <sudden6> jumikan: yes
[18:11:07] <jumikan> Neat
[18:11:19] <sudden6> you?
[18:11:40] <jumikan> I have some interesting ideas that could make Tox ultra popular
[18:12:15] <jumikan> Yes, I graduted in Computer Science in 2012. I work as a PHP web developer
[18:12:54] <jumikan> I am really interested in meeting up with Tox core developers
[18:12:59] <sudden6> cool, what are your ideas?
[18:13:18] <jumikan> are you near the circle of geeks of Tox's core?
[18:13:20] <sudden6> for the core devs, you should also consider #toktok
[18:15:37] <jumikan> OK, the ideas are basically two. you would be able to use Tox not just to protect your privacy but also, if you wanted to expose it sometimes, knowing exactly the risks and assuming responsability for it
[18:16:47] <sudden6> what's the idea behind that?
[18:18:44] <jumikan> You would be able to enter a mode "Publicly Visible to my local GPS neighbours". And also, in another mode, looking for users with simillar likes and dislikes. You could put three likes (hashtags) and three dislikes (antihashtags) like: #philosophy #politics #sports !#religion !#nazism !#football   
[18:20:13] <jumikan> You would have a very strong ARE you Sure? disclaimer when entering the mode "nearby neighbours" and "afinity by hashtags & antihashtags"
[18:20:42] <sudden6> well, for that GPS thing to work, you would basically need to announce your coordinates to the DHT and with this basically to the whole internet
[18:21:05] <sudden6> there should be very very very strong warning...
[18:21:54] <jumikan> I live in London. More precisely in Lambeth, South London. How cool would be to open qTox and be able to see a list sorted by the closest users and ending in the most apart ones and also be able to know their see main likes and dislikes
[18:23:32] <sudden6> I'm not sure Tox is a good platform for that
[18:23:51] <jumikan> Yes, that is what I am saying. You would have security, warnings and disclaimers. You would be told you are chosing a mode where you cannot be sure if others are or not spoofing their real location and saying that you are making yourself exposed to stalking.
[18:23:56] <sudden6> what you describe, sounds more like a social network
[18:26:07] <jumikan> After being altertedm warned and asked ARE YOU SURE? you would have the option to enter this anti-privacy modes that for many could be ultra helpful and promoting the fundamental right of freedom of association and of knowing about your surrondings and giving you tools to better relate with it
[18:27:32] <jumikan> It would just be a list of all users that accepted to be visible in that node, using what the app thought is your real location given your ip address
[18:28:06] <jumikan> ordered by the closest ones first and the others on the other side of the world last
[18:28:30] <jumikan> this would make TOX ultra popular. I am teling you
[18:35:41] <jumikan> With three hashtags and three antihashtags you be giving hints that can make really interesting and intelligent sortings so that you are more likely to connect with people you are interested in meeting, creating more harmony in your world
[18:38:41] <sudden6> jumikan: TBH, personally I don't like this idea very much, especially the broadcasting stuff to everybody part
[18:39:35] <sudden6> you could probably try with some sort of modified groupbot how well your idea is received
[18:40:27] <jumikan> for instance one client having: #cooking #vegan #books !#pasta !#carbs and another: #books #vegan #furniture !#microwave !#netflix
[18:41:42] <jumikan> eventually the network will have intelligence to meet you with others in interesting ways
[19:33:42] *** Quits: jumikan (4f4eb6fe@ ) (Quit: Page closed)
[19:42:02] <Chiitoo> I thought there already are a lot of things that are already close to, if not exactly what they want, heh...
[19:47:38] *** Quits: lmposter (~lmposter@ ) (Read error: Connection reset by peer)
[19:59:59] *** Quits: s-mutin (~s-mutin@ ) (Read error: Connection reset by peer)
[20:00:28] *** Joins: s-mutin (~s-mutin@ )
[20:03:10] *** Joins: murray (~murray@ )
[20:04:09] *** Joins: lmposter (~lmposter@ )
[20:13:37] *** Quits: f10 (~flo@ ) (Ping timeout: 248 seconds)
[20:27:08] *** Joins: cenysor (~cenysor@ )
[20:36:40] -qtox-git-spam- [qTox] Diadlo pushed 1 new commit to master: https://git.io/vdpOD
[20:36:40] -qtox-git-spam- qTox/master 92def83 Diadlo: fix(cameradevice): Save string in local variable to avoid errors
[20:37:36] <Chiitoo> Heh.  Friend getting more and more convinced that qTox is doing something nasty, what with them hundreds of connections to .ru domains and stuff.  >.<
[20:42:46] <Chiitoo> I wonder if disabling UDP will help...  Well, not with the paranoia, but the connections.
[20:45:28] <Chiitoo> Apparently no connection at all without it, so in a way, that fixes it for them.  :V
[21:00:48] *** Joins: f10 (~flo@ )
[21:33:35] *** Quits: murray (~murray@ ) (Quit: Leaving)
[21:49:09] *** Quits: cenysor (~cenysor@ ) (Remote host closed the connection)
[22:04:16] <oranges> you can't solve user stupidity Chiitoo 
[22:04:39] <oranges> it's like pushing your boat against the current
[22:04:48] <oranges> you just get tired
[22:08:40] <Chiitoo> oranges: While that may be true, most users, even not so stupid ones, would probably think hundreds, if not thousands of connections using a lot of bandwidth at least peculiar.  :]
[22:09:01] <Chiitoo> Well of course there's something going awry that it does use that much of bandwidth.
[22:09:05] <oranges> yeah
[22:09:54] <Chiitoo> Gonna try to look into it more at some point.  Now it's naptime point!
[22:12:33] <oranges> you guys are keeping the updater vm up to date right
[22:12:39] <oranges> you guys being the qtox project
[22:19:14] <sudden6> oranges: what updater vm?
[22:19:54] <oranges> the one that the windows qtox client contact to check for updates?
[22:20:04] <oranges> it's been living in my linode account for the past 2 years
[22:20:14] <oranges> I donated it to tux3
[22:20:18] <oranges> 683 days uptime
[22:20:24] <sudden6> oranges: tux3 never gave anyone access to it :/
[22:20:39] <oranges> how have you been pushing out updates then?
[22:21:04] <sudden6> tux3 did it when he found time
[22:21:26] <sudden6> which is getting less and less I think...
[22:23:46] <oranges> hmm
[22:24:36] <oranges> well hopefully he transitions control of the machine gracefully
[22:24:40] <sudden6> are you still in contact with him?
[22:24:43] <oranges> no
[22:24:51] <sudden6> me neither :(
[22:24:55] <oranges> I assumed since he was still here the qtox team was
[22:25:51] <sudden6> I'll write him an email, lets see
[22:26:22] <oranges> tell him if he doesn't respond in a week I'll add a current nominated member of the qtox team to the box
[22:26:39] <sudden6> ok
[22:27:33] <oranges> also is anyone still using the tox jenkins?
[22:27:39] <oranges> I think it would be better to shut it down
[22:28:20] <sudden6> qTox is using it for some nightly builds and some release builds, IDK about uTox though
[22:28:31] <sudden6> why do you think it's better to shut it down?
[22:28:42] <oranges> I assumed nobody was using it
[22:29:01] <oranges> tbh I've been very disconnected from tox
[22:29:04] <oranges> for quite some while
[22:29:42] <sudden6> well our download links point to it for windows nightlies, so if you decide to shut it down I'd appreciate a heads up :D
[22:30:06] <sudden6> I hope you're still interested?
[22:30:34] <oranges> no I am not really, I just happen to be sitting in a position where I have the keys to everything, so I can't really leave
[22:31:22] <oranges> most of the tox group originals are gone or inactive as well, so only nurupo is holding down the fort
[22:31:37] <oranges> and on top of all that our DO credit is running out
[22:33:28] <sudden6> yeah, there's some gap between the devs somehow, somehow a few new groups formed but without real connections to the old group
[22:33:50] <oranges> yes
[22:34:03] <oranges> and most of the client projects are insular
[22:34:40] <sudden6> yeah, but at the toxcon this weekend some plans rised how to improve the core, I hope that will create some new traction
[22:39:23] <sudden6> oranges: I sent him a mail, lets see
[23:13:38] *** Quits: storrgie (~storrgie@ ) (Remote host closed the connection)
[23:16:31] *** Joins: storrgie (~storrgie@ )
[23:26:04] *** Quits: lmposter (~lmposter@ ) (Quit: Leaving)
[23:54:00] *** Quits: f10 (~flo@ ) (Ping timeout: 246 seconds)
