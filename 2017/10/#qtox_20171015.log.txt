[00:04:54] *** Quits: sudden6 (~anonymous@ ) (Ping timeout: 258 seconds)
[00:14:01] *** Quits: cenysor (~cenysor@ ) (Remote host closed the connection)
[00:19:13] *** Joins: MrNePruM (AdiIRC@ )
[01:27:48] *** Parts: MrNePruM (AdiIRC@ ) ("Closing Window")
[01:58:51] <Emcy> qtox video broken?
[02:07:14] *** Quits: f10 (~flo@ ) (Ping timeout: 255 seconds)
[03:02:38] <Emcy> video broken on windows 1.12
[03:54:30] *** Quits: Emcy (~MC@ ) (Ping timeout: 258 seconds)
[04:29:07] *** Joins: Emcy (~MC@ )
[05:42:12] *** Quits: Emcy (~MC@ ) (Read error: Connection reset by peer)
[05:50:59] *** Joins: Emcy (~MC@ )
[08:12:39] *** Joins: lmposter (~lmposter@ )
[08:22:06] <nurupo> you are using the nightly from Jenkins, right?
[08:22:38] <nurupo> do you also happened to use windows 10 with the most recent updates?
[08:23:58] <nurupo> it's probably qtox issue, but i will bump the ffmpeg version on travis from 3.2.6 to 3.2.8 since a newone released a month ago
[08:24:34] <nurupo> oh, right, you won't even be able to test it since qTox nightly fails to build
[08:26:20] <nurupo> pretty sure updating ffmpeg will have close to 0 effect on fixing your issue, though one of windows 10 updates last year *did* break ffmpeg, so you never really know
[09:17:50] *** Joins: murray (~murray@ )
[09:23:10] *** Joins: f10 (~flo@ )
[10:28:37] *** Joins: cenysor (~cenysor@ )
[10:37:42] *** Quits: f10 (~flo@ ) (Ping timeout: 248 seconds)
[11:21:50] *** Quits: murray (~murray@ ) (Ping timeout: 258 seconds)
[11:37:00] *** Joins: f10 (~flo@ )
[11:48:54] -qtox-git-spam- [qTox] noavarice closed pull request #4510: [WIP] Another URL fixing. (master...fix-url-again) https://git.io/vQ6r8
[11:53:49] *** Joins: murray (~murray@ )
[13:08:15] *** Quits: murray (~murray@ ) (Ping timeout: 255 seconds)
[13:18:55] *** Joins: _sudden6_ (~anonymous@ )
[14:11:14] *** Joins: murray (~murray@ )
[16:31:05] *** Joins: Mr_Keyser_Soze (~Mr_Keyser@ )
[17:10:14] <nurupo> travis cache is weird
[17:10:50] <nurupo> still trying to get cache to work correctly
[17:11:16] <nurupo> it's also annoying that i can't stop builds early, i think it requires me being in qtox's group or something
[17:12:31] <_sudden6_> nurupo: yes, I think you have to have write access to the repository
[17:12:51] <_sudden6_> however, the build is also stopped if you push a new commit
[17:13:37] <nurupo> the build doesn't stop when i push a new commit
[17:13:52] <nurupo> whom can i ping if i want to stop a build?
[17:14:20] <nurupo> i don't need it right now, but i needed it earlier
[17:14:42] <nurupo> i guess you, Diadlо and zetоk
[17:14:49] *** _sudden6_ is now known as sudden6
[17:14:54] *** sudden6 is now known as _sudden6_
[17:15:12] *** Quits: lmposter (~lmposter@ ) (Read error: Connection reset by peer)
[17:15:19] <_sudden6_> hmm, a new commit should stop the running build, let me check the config
[17:15:28] <_sudden6_> yes, also noavarice
[17:16:37] <nurupo> well, not in a pr
[17:17:08] <nurupo> actually, now that i think about it, i might have not tried testing it
[17:17:59] <_sudden6_> just looked it up, only the builds in "waiting" state will be canceled, running jobs will finish
[17:20:37] -qtox-git-spam- [qTox] sudden6 pushed 2 new commits to master: https://git.io/vdMmy
[17:20:37] -qtox-git-spam- qTox/master d69023c Vincas Dargis: fix(build): do not build unix-specific test on Windows...
[17:20:37] -qtox-git-spam- qTox/master cba200d sudden6: Merge pull request #4737...
[17:20:45] -qtox-git-spam- [qTox] sudden6 closed pull request #4737: fix(build): do not build unix-specific test on Windows (master...fix-windows-build) https://git.io/vd132
[17:21:32] -qtox-git-spam- [qTox] sudden6 pushed 2 new commits to master: https://git.io/vdMm7
[17:21:32] -qtox-git-spam- qTox/master 7170b48 anthony.bilinski: fix(init): register AV connects and call after AV is ready...
[17:21:32] -qtox-git-spam- qTox/master 6479b2c sudden6: Merge pull request #4651...
[17:23:05] *** Joins: lmposter (~lmposter@ )
[17:37:30] <nurupo> i don't understand how the cache works :\
[17:38:22] <nurupo> x86_64: stage2 for did get stage1's cache
[17:38:22] <nurupo> i686: stage2 got empty cache, nothing from stage1
[17:38:34] <nurupo> gah, broken english
[17:39:08] <nurupo> x86_64: stage2 did get stage1's cache
[17:39:08] <nurupo> i686: stage2 didn't get stage1's cache
[17:39:43] <nurupo> _sudden6_: can you stop https://travis-ci.org/qTox/qTox/builds/288231911 ?
[17:39:59] <_sudden6_> maybe the cache is deleted by the first stage2 build?
[17:40:07] <nurupo> the job will continue for 20 or 30 minutes -_-
[17:40:19] <_sudden6_> cancelled
[17:40:53] <nurupo> afaik the cache doesn't get deleted
[17:41:14] <nurupo> unless you explicitly delete it wither with rm or in Travis's settings
[17:41:35] <_sudden6_> I mean an accidental delete with rm maybe?
[17:41:53] <nurupo> i will try something, but first i need to delete all the cache
[17:42:00] <_sudden6_> was just a wild guess though
[17:43:51] <nurupo> now to wait ~20 min for cached to get deleted https://github.com/qTox/qTox/pull/4736/commits/d863b9380d30907c159f3aeaa16e7523af502d72
[17:44:39] <_sudden6_> nurupo: uhm should I delete the cache via the travis webui?
[17:46:30] <nurupo> that would work too
[17:46:41] <nurupo> cancel the build if you do so
[17:47:36] <_sudden6_> nurupo: both done
[17:47:39] *** _sudden6_ is now known as sudd3n6
[17:47:43] *** sudd3n6 is now known as sudden6
[17:47:44] *** sudden6 is now known as _sudden6_
[17:47:55] <nurupo> thanks
[17:48:33] <nurupo> um, can you cancel the new build i just started too?
[17:48:40] <nurupo> forgot to commit something
[17:49:39] <nurupo> it has been like this for 2 days now: do a change, wait 60 minutes to see if it worked, repeat. that's why i don't like working with Travis :\
[17:49:54] <_sudden6_> this one? https://travis-ci.org/qTox/qTox/builds/288248891?utm_source=github_status&utm_medium=notification
[17:50:23] <nurupo> yeah
[17:50:55] <_sudden6_> nurupo: done
[17:51:16] <nurupo> thanks
[18:48:44] <nurupo> i think i fixed it with my last commit
[18:49:01] <_sudden6_> what was the problem?
[18:49:22] <nurupo> oh, not the latest, the latest was mt commiting the change i forgot to commit before
[18:49:32] <nurupo> this commit https://github.com/qTox/qTox/pull/4736/commits/206a191902cd14ad0477b2dd035f1cd501b420d7
[18:49:57] <nurupo> _sudden6_: no idea what was the problem, but if i have a guess
[18:50:48] <nurupo> if jobs can share cache only if they have the exact same configuration: same `env`s, os, distro, etc.
[18:52:13] <nurupo> the issue was that between i686 and x86_64, one would keep the cache into the 2nd stage while the other wouldn't 
[18:52:48] <_sudden6_> nurupo: seems what you did is the recommended way of fixing this: https://docs.travis-ci.com/user/caching/#Caches-and-build-matrices
[18:54:06] <nurupo> they have the exact same configuration to allow stage 1, 2 and 3 to share the cache, but i686 and x86_64 didn't share cache because they used different cache dirs: /opt/build-windows/i686 and /opt/build-windows/x86_64
[18:55:00] <nurupo> my guess was the the last part of my last message might be false. what is share some global cache store for the same-configuration jobs?
[18:55:17] <_sudden6_> in https://docs.travis-ci.com/user/caching/#How-does-the-caching-work%3F
[18:55:23] <_sudden6_> they mention how it works
[18:55:47] <nurupo> so i forced any possobility of it being shared by changing the configuration of i686 and x86_64 builds
[18:56:04] <_sudden6_> so, I think when the last job of the stage finished, it created a tar whitch overwrote the previous cache
[18:56:16] <nurupo> anyway, Travis cache is such a pain, so many moving parts and things you need to know in order to use it correctly
[18:56:41] <nurupo> _sudden6_: yeah, that's what i though might have happened
[18:57:13] <nurupo> but i also thought that there would be separate tars, one for /opt/build-windows/i686 and one for /opt/build-windows/x86_64
[18:58:03] <nurupo> since i only request the i686 cache in i686 job and x86_64 cache in x86_64 job https://github.com/nurupo/qTox/blob/9fd5f3b797552986a2ed8d11a08f1ac7d10b693a/.travis.yml#L40
[18:58:23] <nurupo> but it seems like they are per-configuration global
[18:58:52] <nurupo> so by making i686 and x86_64 jobs having different configurations, they now use separate tars
[18:59:37] <nurupo> the `env: CACHE_TRICK_VARIABLE=` part is the one that makes their configurations different, they differ by CACHE_TRICK_VARIABLE's value
[18:59:50] <nurupo> even if one env variable differs, your jobs won't share cache
[19:00:20] <nurupo> https://docs.travis-ci.com/user/caching/#Caches-and-build-matrices
[19:00:20] <nurupo> >4. Names and values of visible environment variables set in .travis.yml or Settings panel
[19:00:21] <_sudden6_> mhm, you're right the docs seem to be unclear about different directories and their cache behavior
[19:00:56] <nurupo> someone would have a lot of fun when they modify .travis.yml and accidentally break the caching :D
[19:01:18] <_sudden6_> well, I hope Travis won't pass then anyway^^
[19:01:46] <nurupo> yeah, it probably wouldn't
[19:01:54] <nurupo> probably
[19:02:19] <_sudden6_> what do you think about giving qt and openssl their own build jobs?
[19:04:49] <_sudden6_> on a second thought, that's probably not a good idea, since rebuild won't happen that often
[19:05:13] <nurupo> openssl and qt are already a separate job
[19:05:22] <nurupo> stage 1: openssl and qt
[19:05:27] <nurupo> stage 2: other deps
[19:05:31] <nurupo> stage 3: qtox
[19:05:42] <_sudden6_> I meant stage1 qt, stage2 openssl, but forget about it
[19:06:06] <nurupo> stage 1 can take anywhere from 35 to 47 minutes to run, depending on the machine Travis gives us
[19:06:12] <nurupo> the limit is 50 minutes
[19:06:31] <nurupo> so we have to make openssl and qt a separate from other deps job
[19:06:40] <_sudden6_> yeah, I get
[19:06:41] <nurupo> otherwise we hit the time limit
[19:06:42] <_sudden6_> it
[19:07:41] <nurupo> oh
[19:07:47] <nurupo> you need openssl for qt
[19:08:08] <nurupo> our Qt build uses the OpenSSL we build
[19:08:46] <nurupo> qTox uses Qt to access ToxMe over https, so Qt needs to have OpenSSL support
[19:08:58] <nurupo> and, well, Qt doesn't come with OpenSSL bundled-in
[19:09:14] <_sudden6_> sad that Travis doesn't support a more elaborate dependency mechanism :/
[19:09:22] <nurupo> so you need to cross-compile OpenSSL and tell Qt to use it when building Qt
[19:09:24] <_sudden6_> could save a lot of run and wait time
[19:09:35] <_sudden6_> I see
[19:09:35] <nurupo> yeah
[19:11:57] <nurupo> great, looks like it works https://travis-ci.org/qTox/qTox/builds/288249343
[19:12:14] <nurupo> i actually expected the compilation to fail though
[19:12:31] <nurupo> does my PR branch already include the windows patch?
[19:12:39] <nurupo> i thought you just merged it...
[19:13:41] <nurupo> i don't understand
[19:13:47] <nurupo> https://github.com/nurupo/qTox/commits/travis-windows
[19:17:02] <nurupo> my branch doesn't have the fixed qTox's master has
[19:17:17] <nurupo> yet Jenkins build fails but Travis succeeds
[19:18:09] <nurupo> the only difference is that Jenkins uses Debian Stretch instead of Ubuntu 16.04, Debian has newer mingw compilers
[19:18:59] -qtox-git-spam- [qTox] sudden6 opened pull request #4740: chore(README): recommend the build from Travis CI for Mac OS (master...update_readme) https://git.io/vdMnZ
[19:19:36] <nurupo> let's see what will happen when i change the cross-compilation script to use Debian Stretch
[19:22:05] <nurupo> hopefully the apt-get commands stay the same
[19:22:24] <nurupo> this will also test is modifying build.sh will force everything to be rebuilt
[19:23:03] <nurupo> this should do it https://github.com/qTox/qTox/pull/4736/commits/390b8c30ef591675126f5f1cfe775802b516b8c9
[19:23:25] *** Quits: _sudden6_ (~anonymous@ ) (Ping timeout: 240 seconds)
[19:23:44] <nurupo> now wait 60 minutes, expecting it to fail during the 3rd stage of Windows https://travis-ci.org/qTox/qTox/builds/288272943 :P
[19:25:14] <nurupo> still, i don't understand
[19:25:23] <nurupo> this should have caused it to fail for sure https://github.com/qTox/qTox/pull/4737
[19:25:28] <nurupo> even on older mingw
[19:25:50] <nurupo> just what is going on?
[19:28:06] <nurupo> it just skipped the toxmedata and posixsignalnotifier tests https://travis-ci.org/qTox/qTox/jobs/288249352#L2173
[19:28:50] <nurupo> https://github.com/nurupo/qTox/blob/travis-windows/cmake/Testing.cmake#L24-L25
[19:29:33] <nurupo> why did it skip them?
[19:45:17] <nurupo> can someone answer this? ^ would be very helpful
[19:52:34] <Chiitoo> I definitely can't see 'posixsignalnotifier' either, but I do see [ 82%] Built target test_toxmedata?
[19:52:58] <Chiitoo> Or is that a different test.
[19:55:12] <nurupo> oh, right, it builds test_toxmedata
[19:56:05] <nurupo> but it did skip posixsignalnotifier
[19:56:21] <nurupo> there is no if() around it in my branch
[19:57:01] <nurupo> toxmedata should also have failed due to https://github.com/qTox/qTox/issues/4730
[19:58:40] <nurupo> while mingw and cmake version difference could result in toxmedata failing with one version of mingw but not with another, it's hard to believe that it would also make cmake skip posixsignalnotifier
[19:59:37] <nurupo> can't wait on Travis to finish the build to see the results with Debian Stretch
[20:14:25] *** Quits: Mr_Keyser_Soze (~Mr_Keyser@ ) (Remote host closed the connection)
[20:30:57] <nurupo> zetok: could you cancel https://travis-ci.org/qTox/qTox/builds/288289404 ?
[20:32:47] <zetok> nurupo: cancelled
[20:33:36] <nurupo> thanks
[20:51:56] *** Joins: Xeno_PL (~Xeno@ )
[21:21:59] *** Quits: cenysor (~cenysor@ ) (Remote host closed the connection)
[21:41:02] <nurupo> huh
[21:44:05] <nurupo> not sure why posixsignalnotifier isn't built on Travis with Debian Jessie and Ubuntu 16.04, yet it builds on Jenkins
[22:01:51] *** Joins: sudden6 (~anonymous@ )
[22:11:24] *** Quits: murray (~murray@ ) (Quit: Leaving)
[23:03:15] *** Quits: lmposter (~lmposter@ ) (Ping timeout: 248 seconds)
[23:21:59] <nurupo> should i keep the cross-compile script build Qt 5.6.3 or should i make it build Qt 5.9.2?
[23:22:17] <nurupo> is there any benefit in using 5.9.2?
[23:23:10] <nurupo> not sure how it will affect the build time, which we already getting close to the limit on Qt build on Travis
[23:28:20] <Chiitoo> Hmmm, from a quick look at just Qt Core, there hasn't been a big change.  Would of course need to check all the modules that are used.
[23:37:23] *** Quits: Xeno_PL (~Xeno@ ) (Ping timeout: 248 seconds)
[23:48:53] <nurupo> Qt added lots of new modules between 5.6 and 5.9
[23:49:42] <nurupo> text to speech, purchasing, 3d, charts, gamepad, etc.
[23:50:17] <nurupo> qTox can't use anything that's newer than Qt 5.4 or something like this
[23:51:29] <nurupo> virtualkeyboard - might be useful for password login
[23:53:49] <Chiitoo> Mhm.
