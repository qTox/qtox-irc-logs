[00:12:30] *** Quits: murray (~murray@ ) (Read error: Connection reset by peer)
[02:09:49] *** Quits: M6HZ (~M6HZ@ ) (Read error: Connection reset by peer)
[02:46:42] *** Quits: sudden6 (~anonymous@ ) (Ping timeout: 248 seconds)
[02:59:42] *** Quits: quba42 (~quba42@ ) (Ping timeout: 246 seconds)
[03:11:51] *** Joins: quba42 (~quba42@ )
[03:21:56] *** Quits: cisc (~cisc@ ) (Ping timeout: 248 seconds)
[03:26:58] *** Joins: cisc (~cisc@ )
[03:58:21] *** Quits: quba42 (~quba42@ ) (Ping timeout: 276 seconds)
[04:10:17] *** Joins: quba42 (~quba42@ )
[05:09:15] *** Quits: f10 (~flo@ ) (Ping timeout: 240 seconds)
[07:50:06] *** Joins: murray (~murray@ )
[08:02:34] *** Joins: lmposter (~lmposter@ )
[08:14:32] *** Joins: sudden6 (~anonymous@ )
[08:31:47] *** Quits: cisc (~cisc@ ) (Ping timeout: 248 seconds)
[08:36:09] *** Joins: cisc (~cisc@ )
[09:17:00] *** Quits: LittleVulpix (~Miranda@ ) (Quit: Miranda NG rocks. Trust the Vulpix. http://miranda-ng.org/)
[09:35:09] *** Quits: murray (~murray@ ) (Ping timeout: 252 seconds)
[09:39:05] *** Joins: LittleVulpix (~Miranda@ )
[09:48:15] *** Joins: noavarice (~alexrazin@ )
[09:56:06] *** Quits: VVD (~VVD@ ) (Remote host closed the connection)
[10:14:45] *** Joins: murray (~murray@ )
[10:49:32] *** Quits: quba42 (~quba42@ ) (Quit: Leaving)
[12:55:15] *** Joins: VVD (~VVD@ )
[13:39:55] *** Quits: noavarice (~alexrazin@ ) (Quit: WeeChat 1.5)
[13:59:58] *** Quits: igajsin (~ugoday@ ) (Quit: Leaving.)
[14:02:58] *** Joins: ghutzriop_ (~ghutzriop@ )
[14:06:19] *** Quits: LittleVulpix (~Miranda@ ) (Read error: Connection reset by peer)
[14:09:47] *** Joins: LittleVulpix (~Miranda@ )
[14:20:46] *** Quits: s-mutin (~s-mutin@ ) (Ping timeout: 255 seconds)
[14:42:26] *** Quits: VVD (~VVD@ ) (Read error: Connection reset by peer)
[14:43:19] *** Joins: VVD (~VVD@ )
[15:09:17] *** Joins: f10 (~flo@ )
[15:10:38] *** Quits: VVD (~VVD@ ) (Read error: Connection reset by peer)
[15:11:20] *** Joins: VVD (~VVD@ )
[16:17:44] <ghutzriop_> so, I'm looking to contribute qtox, maybe someone can help me with the first steps, so I'm not making any mistakes
[16:32:07] *** Joins: Anko (~Anko@ )
[16:52:56] <sudden6> hi ghutzriop_ what exactly do you want to know? :)
[16:54:43] <sudden6> If you want to directly start with an issue, https://github.com/qTox/qTox/issues/4552 may be a good one, to get into the workflow
[16:58:47] <ghutzriop_> sudden6, not really sure about pull requests, especially because I never made any on github. I already did exactly that one.
[16:59:50] *** ghutzriop_ is now known as ghutzriop
[17:00:34] <sudden6> ok, if you already did that, just take a look at https://github.com/qTox/qTox/blob/master/CONTRIBUTING.md#commit
[17:00:51] <sudden6> it contains some rules how to format commit messages
[17:02:15] <sudden6> ghutzriop: then just fork the qTox repo to your own, upload your changes as a branch and click the "create pull request" button :)
[17:02:50] <ghutzriop> sudden6, would that be correct as the commit message?     fix(i18n): [Regression] string `Call with %1 ended. %2` is no longer translatable\ \ Fixes #4552
[17:03:51] <sudden6> I would leave "[Regression]" out of it, but it's not against the rules
[17:05:43] <ghutzriop> sudden6, and the translation update script should be a different commit, right?
[17:06:00] <sudden6> translation update script`
[17:06:01] <sudden6> ?
[17:07:36] <ghutzriop> sudden6, tools/update-translation-files.sh
[17:07:41] <sudden6> ah
[17:08:13] <sudden6> I don't think the script has to be run
[17:08:42] <sudden6> zetok: ↑ do we need to run the script?
[17:08:49] -qtox-git-spam- [qTox] kkaufmann opened pull request #4595: fix(i18n): string `Call with %1 ended. %2` is no longer translatable (master...4552) https://git.io/v53TS
[17:11:33] <sudden6> ghutzriop: PR looks good
[17:15:52] <ghutzriop> sudden6, good to hear
[17:17:10] <sudden6> ghutzriop: if you have any questions about anything you can always ask here in the IRC or over Tox
[17:18:40] <ghutzriop> sudden6, thanks
[17:19:33] <sudden6> all the devs have also the same names here as on github, don't hesitate to ping us if it's urgent :)
[17:27:57] <ghutzriop> does the tag "proposal-accepted" on issues mean, that you want the issue fixed/implemented?
[17:33:43] <sudden6> ghutzriop: yes
[18:41:45] *** Quits: cisc (~cisc@ ) (Ping timeout: 260 seconds)
[18:45:43] *** Joins: cisc (~cisc@ )
[19:26:33] *** Quits: Anko (~Anko@ ) (Remote host closed the connection)
[19:49:43] *** Quits: cisc (~cisc@ ) (Quit: Konversation terminated!)
[20:20:20] *** Joins: cisc (~cisc@ )
[20:36:32] *** Quits: sudden6 (~anonymous@ ) (Ping timeout: 248 seconds)
[20:37:00] *** Joins: sudden6 (~anonymous@ )
[21:22:48] *** Quits: cisc (~cisc@ ) (Ping timeout: 246 seconds)
[21:27:58] *** Joins: cisc (~cisc@ )
[21:36:16] *** Quits: f10 (~flo@ ) (Ping timeout: 248 seconds)
[21:43:49] *** Joins: f10 (~flo@ )
[21:54:56] *** Quits: lmposter (~lmposter@ ) (Ping timeout: 248 seconds)
[21:58:26] *** Joins: lmposter (~lmposter@ )
[22:02:19] *** Joins: noavarice (~alexrazin@ )
[22:23:43] *** Quits: noavarice (~alexrazin@ ) (Quit: WeeChat 1.5)
[23:08:54] *** Quits: lmposter (~lmposter@ ) (Ping timeout: 260 seconds)
[23:41:26] *** Quits: ghutzriop (~ghutzriop@ ) (Ping timeout: 255 seconds)
[23:48:47] *** Quits: murray (~murray@ ) (Quit: Leaving)
[23:54:53] *** Quits: sudden6 (~anonymous@ ) (Ping timeout: 240 seconds)
