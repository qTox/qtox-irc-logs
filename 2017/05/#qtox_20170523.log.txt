[00:16:47] <nurupo> splatree: Mike34: there is no portable qTox
[00:17:16] <nurupo> it's just a zipped qTox, one without an installer
[00:17:30] <Mike34> nurupo, for windows you simply and unzie and then run the .exe file to run it?
[00:17:36] <Mike34> unzip
[00:17:37] <nurupo> it still stores all its files in %appdata%
[00:17:52] <Mike34> it can be run anywhere?
[00:18:23] <nurupo> so if you put it on a usb stick and create a tox profile on one computer, the profile won't be stored on the usb stick
[00:18:32] <Mike34> i c
[00:18:37] <Mike34> so just run the exe file right?
[00:18:45] <Mike34> after unzip
[00:18:54] <nurupo> yes
[00:19:06] <Mike34> thanks
[00:19:54] *** Quits: Achylles (~Achylles@ ) (Ping timeout: 246 seconds)
[00:20:00] <nurupo> btw, it doesn't have auto-updater, so it won't update to a new version
[00:26:06] <Mike34> nurupo, for the linux version is there a particular reason that the ubuntu version is something like kubuntu or something
[00:27:05] <nurupo> ubuntu and kubuntu linux distributions have the same versions
[00:27:38] <nurupo> kubuntu is pretty much just ubuntu with plasma/kde
[00:28:17] <Mike34> your right ..i was curious
[00:28:19] <nurupo> you might want to join #ubuntu for such questions
[00:28:24] <Mike34> they are practically the same
[00:28:42] <Mike34> no i was here because that is the version it was built for
[00:29:01] <Mike34> usually ubuntu programs say ubuntu not kubuntu
[00:34:01] *** Joins: Achylles (~Achylles@ )
[00:48:34] *** Joins: f10_ (~flo@ )
[01:46:42] *** Quits: f10_ (~flo@ ) (Ping timeout: 246 seconds)
[03:34:22] *** Quits: Achylles (~Achylles@ ) (Quit: Leaving)
[03:38:59] *** Parts: Mike34 (~Mike34@ ) ("Leaving")
[03:52:30] *** Joins: dardevelin (~dardeveli@ )
[03:59:57] *** Joins: SSBN-731 (~Alabama@ )
[04:04:30] <splatree> ><nurupo> splatree: Mike34: there is no portable qTox
[04:04:30] <splatree> so, what does the 'make tox portable' tickbox, under the 'portable' heading, in the 'advanced' tab, of settings, do? nothing?
[04:07:01] <nurupo> splatree: you are taking my messages out of context. Mike was asking if the nightly qTox .zip is the portable version of the stable qTox that comes with an installer
[04:07:10] <nurupo> so what i meant is that there is no pre-configured portable download of qtox that i know of
[04:10:14] <nurupo> splatree: that's a nice catch though
[04:10:22] <splatree> to be fair, i don't think mike34 was confusing stable and nightly. i imagine they were simply wondering how to use the zip.
[04:11:41] <nurupo> i'd imagine moving the stable qTox's file on a usb drive and configuring it to run in a portable mode would make it portable and auto-updatable
[04:19:27] <splatree> if mike34 comes back, you might want to clarify that it's only the nightly that doesn't autoupdate. that might not have been clear.
[05:09:38] *** Quits: SSBN-731 (~Alabama@ ) (Quit: Dive! Dive!)
[07:30:15] *** Quits: dardevelin (~dardeveli@ ) (Remote host closed the connection)
[07:30:46] *** Joins: dardevelin (~dardeveli@ )
[07:52:18] *** Joins: sudden6 (~anonymous@ )
[08:03:46] *** Joins: f10_ (~flo@ )
[08:17:58] *** Joins: Emcy_ (~MC@ )
[08:17:59] *** Quits: Emcy_ (~MC@ ) (Changing host)
[08:17:59] *** Joins: Emcy_ (~MC@ )
[08:21:25] *** Quits: Emcy (~MC@ ) (Ping timeout: 260 seconds)
[08:39:36] *** Quits: f10_ (~flo@ ) (Ping timeout: 255 seconds)
[09:14:54] *** Joins: f10_ (~flo@ )
[09:42:28] *** Quits: f10_ (~flo@ ) (Ping timeout: 240 seconds)
[09:46:01] *** Quits: SpEcHiDE (spechidedi@ ) (Remote host closed the connection)
[09:46:03] *** Quits: Guest98808 (spechideri@ ) (Remote host closed the connection)
[09:46:04] *** Quits: Nolz[m]1 (nolzmatrix@ ) (Read error: Connection reset by peer)
[09:46:06] *** Quits: M-gdr (gdrmatrixo@ ) (Read error: Connection reset by peer)
[09:48:58] *** Joins: Nolz[m] (nolzmatrix@ )
[09:52:28] *** Joins: M-gdr (gdrmatrixo@ )
[09:52:28] *** Joins: SpEcHiDe (spechideri@ )
[09:52:28] *** Joins: SpEcHiDeR (spechidedi@ )
[09:52:40] *** Joins: allectro (b9630855@ )
[10:05:58] *** Quits: _root_ (~root@ ) (Remote host closed the connection)
[10:24:16] *** Quits: allectro (b9630855@ ) (Quit: Page closed)
[10:41:15] *** Quits: cisc (~cisc@ ) (Ping timeout: 258 seconds)
[10:57:50] *** Quits: sudden6 (~anonymous@ ) (Ping timeout: 255 seconds)
[11:13:00] *** Joins: f10_ (~flo@ )
[13:09:16] *** Quits: Nolz[m] (nolzmatrix@ ) (Remote host closed the connection)
[13:09:17] *** Quits: SpEcHiDeR (spechidedi@ ) (Remote host closed the connection)
[13:09:18] *** Quits: SpEcHiDe (spechideri@ ) (Remote host closed the connection)
[13:09:23] *** Quits: M-gdr (gdrmatrixo@ ) (Read error: Connection reset by peer)
[13:11:49] *** Quits: linuxmodder (linuxmodde@ ) (Ping timeout: 260 seconds)
[13:13:30] *** Joins: Nolz[m] (nolzmatrix@ )
[13:14:33] *** Joins: linuxmodder (linuxmodde@ )
[13:16:49] *** Joins: SpEcHiDeR (spechidedi@ )
[13:16:49] *** Joins: M-gdr (gdrmatrixo@ )
[13:16:49] *** Joins: SpEcHiDe (spechideri@ )
[13:33:36] *** Quits: nurupo (~nurupo.ga@ ) (Ping timeout: 240 seconds)
[13:51:29] *** Quits: linuxmodder (linuxmodde@ ) (Changing host)
[13:51:29] *** Joins: linuxmodder (linuxmodde@ )
[15:12:46] *** Joins: rokups (uid197268@ )
[15:56:21] *** Joins: cisc (~cisc@ )
[18:02:28] *** Quits: rokups (uid197268@ ) (Quit: Connection closed for inactivity)
[18:13:34] *** Joins: Achylles (~Achylles@ )
[18:50:28] *** Quits: cisc (~cisc@ ) (Ping timeout: 240 seconds)
[18:55:35] *** Joins: cisc (~cisc@ )
[21:25:17] *** Quits: LittleVulpix (~Miranda@ ) (Quit: Miranda NG rocks. Trust the Vulpix. http://miranda-ng.org/)
[21:28:30] *** Joins: LittleVulpix (~Miranda@ )
[22:37:03] *** Quits: LittleVulpix (~Miranda@ ) (Ping timeout: 255 seconds)
[23:25:29] *** Quits: linuxmodder- (linuxmodde@ ) (Ping timeout: 240 seconds)
[23:31:19] *** Joins: linuxmodder- (linuxmodde@ )
[23:48:28] *** Joins: SSBN-731 (~Alabama@ )
