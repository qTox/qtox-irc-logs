[00:24:05] *** Quits: cenysor (~cenysor@ ) (Remote host closed the connection)
[00:32:36] *** Joins: x_ (~x@ )
[00:38:01] *** Quits: x_ (~x@ ) (Quit: Leaving)
[00:40:11] *** Quits: LittleVulpix (~Miranda@ ) (Quit: Miranda NG rocks. Trust the Vulpix. http://miranda-ng.org/)
[00:41:14] *** Joins: LittleVulpix (~Miranda@ )
[00:53:06] *** Joins: VVD (~VVD@ )
[03:14:08] *** Quits: vindelschtuffen (~vindelsch@ ) (Ping timeout: 246 seconds)
[04:32:07] *** Quits: koshii (~w@ ) (Ping timeout: 268 seconds)
[06:04:01] *** Joins: koshii (~w@ )
[06:13:16] *** Joins: f10 (~flo@ )
[06:13:33] *** Joins: f10_ (~flo@ )
[06:20:23] *** Joins: reBrain (~quassel@ )
[07:35:19] *** Quits: reBrain (~quassel@ ) (Remote host closed the connection)
[08:58:20] *** Quits: f10 (~flo@ ) (Remote host closed the connection)
[09:01:38] *** Joins: f10 (~flo@ )
[09:50:17] *** Quits: voyager_ (~voyager@ ) (Quit: Leaving)
[09:50:40] *** Joins: voyager_ (~voyager@ )
[10:02:19] *** Quits: f10 (~flo@ ) (Ping timeout: 258 seconds)
[10:02:57] *** Quits: f10_ (~flo@ ) (Ping timeout: 260 seconds)
[10:35:31] *** Joins: IRCFrEAK (~gk.1wm.su@ )
[10:35:53] *** Quits: IRCFrEAK (~gk.1wm.su@ ) (K-Lined)
[11:29:08] *** Quits: LittleVulpix (~Miranda@ ) (Quit: Miranda NG rocks. Trust the Vulpix. http://miranda-ng.org/)
[11:55:18] *** Joins: LittleVulpix (~Miranda@ )
[12:31:39] *** Joins: silviof (~silviof@ )
[12:31:49] *** Quits: voyager_ (~voyager@ ) (Ping timeout: 260 seconds)
[12:32:12] *** Joins: voyager_ (~voyager@ )
[13:42:12] *** Joins: f10 (~flo@ )
[13:42:17] *** Joins: f10_ (~flo@ )
[14:24:37] *** Joins: d0rp3d (b2a2d3d5@ )
[14:24:43] <d0rp3d> hi folks
[14:24:53] <d0rp3d> anyone awaked?
[14:26:13] <d0rp3d> I'm trying to compile qtox
[14:27:39] <d0rp3d> however the compilation fails due to a failed dependency
[14:27:41] <d0rp3d> SQLCIPHER package, library or framework not found
[14:28:00] <d0rp3d> however I did compile sqlcipher by hand 
[14:28:17] <d0rp3d> and set the required path on locallib 
[14:28:22] <d0rp3d> but that does not work
[14:28:25] <d0rp3d> any ideas?
[14:28:31] <d0rp3d> the sqlcipher is installed
[14:45:08] <zetok> d0rp3d: what OS?
[14:45:46] <d0rp3d> fedora 24
[14:46:27] <d0rp3d> the sqlcipher lib is there - /usr/local/lib/libsqlcipher.a
[14:47:16] <zetok> did you do `ldconfig` ?
[14:47:19] <d0rp3d> yup
[14:48:16] <d0rp3d> cat /etc/ld.so.conf.d/locallib.conf 
[14:48:17] <d0rp3d>  /usr/local/lib/
[14:48:34] <d0rp3d> and after I did the ldconfig
[14:49:09] <zetok> https://admin.fedoraproject.org/pkgdb/package/rpms/sqlcipher/ ← according to this sqlcipher is available on fedora 24, and there's no need to compile?
[14:50:26] <zetok> d0rp3d: do you have sqlcipher-devel installed?
[14:55:29] <d0rp3d> the oficial fedora 24 doesn't have the sqlcipher
[14:55:47] <zetok> err?
[14:55:52] <d0rp3d> yup
[14:56:09] <d0rp3d> actually the only packages available for sqlcipher are the ones provided by the qtox repo
[14:56:22] <d0rp3d> dnf install sqlcipher-devel Last metadata expiration check: 0:52:06 ago on Sat Mar 11 15:02:17 2017. Dependencies resolved. ===============================================================================================================================================================================  Package                                        Arch                              Version                               Repository        
[14:56:58] <d0rp3d> Installing:  tox-sqlcipher         tox-sqlcipher-devel - repo home_antonbatenev_tox 
[14:58:33] <d0rp3d> which doesn't produce any different outcome btw
[14:58:42] <d0rp3d> I've just tried again to compile qtox
[14:59:04] <d0rp3d> with the sqlcipher binaries from home_antonbatenev_tox
[14:59:21] <d0rp3d> however the cmake produces the same dependency error
[15:01:47] <zetok> how come that it's not available even though website says that it is?
[15:02:06] <d0rp3d> I'm using fedora 24 over qubes
[15:02:11] <d0rp3d> so they have their own repos
[15:02:16] <zetok> k
[15:03:43] <d0rp3d> ah
[15:03:46] <d0rp3d> I know now
[15:03:54] <d0rp3d> https://apps.fedoraproject.org/packages/sqlcipher
[15:04:13] <d0rp3d> they don't have a stable release for sqlcipher
[15:04:23] <d0rp3d> this is only available in testing
[15:04:40] <d0rp3d> that's why it's not available, although the repos are the same in this case (just checked)
[15:09:05] *** Quits: voyager_ (~voyager@ ) (Ping timeout: 260 seconds)
[15:09:55] *** Joins: voyager_ (~voyager@ )
[15:12:36] <d0rp3d> zetok: any idea?
[15:13:21] <zetok> you could run `bootstrap.sh` and see whether it would help
[15:14:28] *** Quits: voyager_ (~voyager@ ) (Ping timeout: 240 seconds)
[15:15:12] <d0rp3d> what does it does?
[15:15:21] <d0rp3d> oh
[15:15:23] <zetok> ./bootstrap.sh -h
[15:15:28] <d0rp3d> its building the entire thing again
[15:16:15] <d0rp3d> it works :)
[15:16:25] <d0rp3d> qtox is now compiled
[15:16:30] <d0rp3d> zetok: many thanks
[15:16:36] <zetok> you're welcome
[15:17:14] <d0rp3d> although it remains unclear why compiling by hand failed
[15:17:22] <d0rp3d> and through the bootstrap worked
[15:17:50] <d0rp3d> now the next step is to wrap this into a rpm
[15:18:11] <d0rp3d> do you know where is the script and spec files for this?
[15:18:12] <zetok> https://github.com/qTox/qTox/blob/master/bootstrap.sh#L163,L168
[15:18:50] <zetok> are you working on a package for fedora?
[15:19:22] <d0rp3d> trying to, yes
[15:19:42] <zetok> awesome
[15:19:45] <zetok> d0rp3d: https://github.com/abbat/tox.pkg/blob/master/qtox/spec.template
[15:20:05] <d0rp3d> don't celebrate yet
[15:20:10] <d0rp3d> let me see how this goes :)
[15:20:26] <d0rp3d> there's no guarantee of sucess
[15:20:29] <d0rp3d> btw
[15:20:48] <d0rp3d> I was following the compilation of sqlcipher through here - https://github.com/qTox/qTox/blob/master/INSTALL.md#sqlcipher
[15:21:06] <d0rp3d> which does not pass the LDFLAGS
[15:21:21] <zetok> mm
[15:21:56] <d0rp3d> dunno if was due to this that I was not able to compile qtox
[15:22:05] <d0rp3d> but that was the only visible difference
[15:22:15] <zetok> then probably that's it
[15:22:24] <zetok> mind making a PR with the change?
[15:23:31] <d0rp3d> not really, no
[15:23:39] <d0rp3d> anyone is allowed fo do PR ?
[15:23:52] <zetok> yep
[15:23:56] <d0rp3d> nice
[15:27:20] *** Joins: cenysor (~cenysor@ )
[16:11:57] *** Joins: voyager_ (~voyager@ )
[16:19:14] <d0rp3d> ...pff
[16:19:34] <d0rp3d> so after all qtox doesn't compile, even with the bootstrap.sh
[16:19:50] <d0rp3d> it sets the whole dependency base
[16:20:16] <d0rp3d> but when I try to compile qtox it still complains about the sqlcipher
[16:27:02] *** Joins: sudden6 (~anonymous@ )
[16:27:21] <zetok> huh
[16:32:05] <d0rp3d> qTox]$ cmake . -- using ccache -- Checking for one of the modules 'sqlcipher' CMake Error at cmake/Dependencies.cmake:72 (message):   SQLCIPHER package, library or framework not found Call Stack (most recent call first):   cmake/Dependencies.cmake:91 (search_dependency)   CMakeLists.txt:75 (include)   -- Configuring incomplete, errors occurred! See also "/home/user/Downloads/T/qTox/CMakeFiles/CMakeOutput.log".
[16:32:26] <d0rp3d> I believe cmake is not looking on /usr/local/lib
[16:32:35] <d0rp3d> where the sqlcipher is installed on
[16:33:04] <zetok> right
[16:34:25] <d0rp3d> any ideas?
[16:34:45] <zetok> does the sqlcipher install .pc files under /usr/local/lib ?
[16:36:58] <d0rp3d> nope
[16:38:16] <zetok> hmm
[16:39:52] <zetok> cmake is using pkg-config to look for deps
[16:39:55] *** Joins: noavarice (5f200ef6@ )
[16:40:46] <zetok> and pkg-config probably doesn't know about sqlcipher in /usr/local
[16:41:14] <zetok> if there were .pc files there, one could point pkg-config to them
[16:41:14] <d0rp3d> is there a way to include that lib into pkg-config ?
[16:41:32] <d0rp3d> I do have a .pc file that was installed along with the rpms
[16:41:47] <d0rp3d> I can move that to the /usr/local/lib directory
[16:42:03] <zetok> um, those will point to where stuff form rpms is
[16:43:04] <d0rp3d> I also found another .pc file on the src director of the sqlcipher
[16:44:20] <d0rp3d> I moved to /usr/local/lib but it makes no difference
[16:45:36] *** Quits: cenysor (~cenysor@ ) (Remote host closed the connection)
[16:53:36] <d0rp3d> its solved
[16:53:54] <d0rp3d> the PKG_CONFIG_PATH made the trick
[17:00:28] *** Quits: noavarice (5f200ef6@ ) (Quit: Page closed)
[18:11:49] *** Quits: sudden6 (~anonymous@ ) (Ping timeout: 240 seconds)
[18:21:44] <zetok> good to know
[18:22:08] <d0rp3d> rpm is already built
[18:22:18] <zetok> \o/
[18:22:25] <d0rp3d> have to change the spec file completely and create my own
[18:23:12] <zetok> btw, sqlcipher should have installed those .pc files, any idea why it didn't?
[18:25:15] <d0rp3d> actually it did
[18:25:35] <d0rp3d> under the /usr/local/lib/pkgconfig/
[18:25:49] <d0rp3d> but pkg-config was unable to find it
[18:26:18] <d0rp3d> so I have to set the PKG_CONFIG_PATH
[18:58:35] *** Quits: d0rp3d (b2a2d3d5@ ) (Quit: Page closed)
[19:10:30] *** Joins: noavarice (5f200ef6@ )
[19:27:42] *** Quits: noavarice (5f200ef6@ ) (Quit: Page closed)
[20:35:45] *** Quits: f10_ (~flo@ ) (Ping timeout: 260 seconds)
[20:35:45] *** Quits: f10 (~flo@ ) (Ping timeout: 260 seconds)
[20:49:23] *** Joins: Fort1 (~Fort1@ )
[20:51:06] <Fort1> Hello, when can I expect qtox update for Debian? It is 2-3 releases behind Win...
[20:59:53] *** Joins: f10 (~flo@ )
[21:00:49] *** Joins: f10_ (~flo@ )
[22:00:11] <zetok> Fort1: https://qtox.github.io/blog/2017/02/15/Maintainer-Wanted.html
[22:55:49] <Fort1> @zetok, thanks, I'll have a look at it, maybe I'll be able to help
[22:59:49] *** Quits: Fort1 (~Fort1@ ) (Quit: Ex-Chat)
[23:23:28] *** Quits: f10_ (~flo@ ) (Ping timeout: 240 seconds)
[23:23:58] *** Quits: f10 (~flo@ ) (Ping timeout: 240 seconds)
